package com.fly;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class HttpBootApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(HttpBootApplication.class, args);
    }
}
